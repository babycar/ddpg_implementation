# from collections import deque
import numpy as np
import random
from ddpg_implementation.rpm import rpm

import pickle as pickle

# replay buffer per http://pemami4911.github.io/blog/2016/08/21/ddpg-rl.html
class combine_rpm(object):
    #replay memory
    def __init__(self, buffer_size):
        self.buffer_size = buffer_size
        self.memory_agent =  rpm(buffer_size)
        self.memory_demo = rpm(buffer_size)
        self.agent_pathname= "agent_memory.pkl"
        self.demo_pathname = "demo_memory.pkl"
        self.index = 0

        import threading
        self.lock = threading.Lock()
        
    def append_demo(self, obj):
        self.memory_demo.append(obj)
        return

    def append(self, obj):
        self.memory_agent.append(obj)
        return

    def size(self):
        return self.memory_agent.size() + self.memory_demo.size()

    def sample_batch(self, batch_size):
        '''
        batch_size specifies the number of experiences to add
        to the batch. If the replay buffer has less than batch_size
        elements, simply return all of the elements within the buffer.
        Generally, you'll want to wait until the buffer has at least
        batch_size elements before beginning to sample from it.
        '''

        self.sample_agent = self.memory_agent.sample_batch(batch_size//2)
        self.sample_demo = self.memory_demo.sample_batch(batch_size // 2)

        return np.concatenate([self.sample_agent[0], self.sample_demo[0]]), \
               np.concatenate([self.sample_agent[1], self.sample_demo[1]]), \
               np.concatenate([self.sample_agent[2], self.sample_demo[2]]), \
               np.concatenate([self.sample_agent[3], self.sample_demo[3]]), \
               np.concatenate([self.sample_agent[4], self.sample_demo[4]])

    
    def save(self, pathname):
        self.memory_agent.save(pathname+"/"+self.agent_pathname)
        self.memory_demo.save(pathname+"/"+self.demo_pathname)
        
    def load(self, pathname):
        self.memory_agent.load(pathname+"/"+self.agent_pathname)
        self.memory_demo.load(pathname+"/"+self.demo_pathname)
